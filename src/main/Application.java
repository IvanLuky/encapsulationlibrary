package main;

import provider.Asteroid;
import provider.NasaDataProvider;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

/**
 * Created by Lidia on 14.03.2020.
 */
public class Application {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter start & end dates in YYYY-MM-DD");
        String start = in.next(), end = in.next();
        //new NasaDataProvider().getNeoAsteroids(start, end);
        test1(start,end);

    }
    public static void test1(String start, String end)throws Exception{
        new NasaDataProvider().getNeoAsteroids(start, end);
    }
}
